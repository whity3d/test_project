var express = require('express')
var  bodyParser = require('body-parser')
var app = express()

var jsonParser = bodyParser.json()

var users = [
    {
        id: 1,
        nickname: "rostishik",
        phone: "+0000000001"
    },
    {
        id: 2,
        nickname: "Whity3D",
        phone: "+0000000002"
    },
    {
        id: 3,
        nickname: "arcfire1",
        phone: "+0000000003"
    },
    {
        id: 4,
        nickname: "aqwrel",
        phone: "+0000000004"
    },
    {
        id: 5,
        nickname: "Veteran4eg",
        phone: "+0000000005"
    }
]

function findUser(id){ 
    var arrUser = users.filter(function(user){
            return user.id == id
        })
    return user = arrUser.shift()
}

app.use('/', express.static(__dirname + "/public"))



app.post('/getUser', jsonParser, function (req, res) {
    if(!req.body) return res.sendStatus(400)
    res.json(findUser(req.body.userId))
})

app.get('/getUser/:userId',function(req,res){
    res.send("userId: " + req.params["userId"])
})

app.listen(3000, function(){
    console.log('example app listening port 3000')
})